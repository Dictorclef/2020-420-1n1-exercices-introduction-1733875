package ca.cegepdrummond;

import java.util.Scanner;

public class Serie10_Math {
    /*
     * Modifiez le code afin d'afficher la valeur minimale entrée
     */
    public void math1() {
        Scanner s = new Scanner(System.in);
        int valeur1 = s.nextInt();
        int valeur2 = s.nextInt();
        System.out.println(Math.min(valeur1,valeur2));

    }

    /*
     * Modifiez le code afin d'afficher la valeur maximale de la valeur absolue des entrées
     *
     * Exemple:
     * -3 -4
     * affichera: 4
     */
    public void math2() {
        Scanner s = new Scanner(System.in);
        int valeur1 = s.nextInt();
        int valeur2 = s.nextInt();
        System.out.println(Math.max(Math.abs(valeur1),Math.abs(valeur2)));
    }
}
