package ca.cegepdrummond;

import java.util.Scanner;

public class Serie11_Fonctions {
    /*
     * Modifiez le code afin que la fonction conversionFaenheitVersCelsius_avec_double retourne la température celsius
     * équivalente à la température en fahrenheit passée en paramètre.
     *
     * Remarqué que la fonction recoit et retourne un double.
     * La formule de conversion est:  c=(f - 32) / 1.8
     */
    public void fonction1() {
        Scanner s = new Scanner(System.in);
        double valeur = s.nextDouble();
        System.out.println(conversionFahrenheitVersCelsius_avec_double(valeur));
    }

    private double  conversionFahrenheitVersCelsius_avec_double(double f) {
        return ((f-32)*5/9);
    }


    /*
     * Comme pour le numéro précédent, modifiez le code afin que la fonction conversionFaenheitVersCelsius_avec_float
     *  retourne la température celsius équivalente à la température en fahrenheit passée en paramètre.
     *
     * Mais cette fois-ci, la fonction doit recevoir et retourner un float.
     *
     * La formule de conversion est:  c=(f - 32) / 1.8
     *
     * Note: pour les plus avancés: oui, il serait possible d'avoir 2 fonctions avec le même nom et se
     *       servir de la signature pour les différencier... mais vous verrez ca dans les cours de
     *       programmation orienté objet.
     */
    public void fonction2() {
        Scanner s = new Scanner(System.in);
        float valeur = s.nextFloat();
        System.out.println(conversionFahrenheitVersCelsius_avec_float(valeur));
    }

    private float  conversionFahrenheitVersCelsius_avec_float(float f) {
        return  ((f-32) / 1.8f);
    }



    /*
     * Modifiez le code afin que la méthode fibo() affiche les nombres de la suite de Fibonacci sur une ligne.
     *
     * Pour la définition de la suite de Fibonacci, voir https://fr.wikipedia.org/wiki/Suite_de_Fibonacci
     *
     * Le nombre en entrée indique combien d'Ééléments de la suite il faut afficher.
     *
     * Le nombre minimum d'éléments à afficher est 3 (vous n'avez pas a faire de validation. Si l'usager
     * entre un nombre < 3, afficher ce que vous pouvez. Il n'y aura pas de tests avec une valeur < 3).
     *
     * Exemple:
     * 3
     * affichera:
     * 0 1 1
     *
     * Exemple 2:
     * 10
     * affichera:
     * 0 1 1 2 3 5 8 13 21 34
     */
    public void fonction3() {
        Scanner s = new Scanner(System.in);
        int valeur = s.nextInt();
        fibo(valeur); // appelez la fonction fibo() ici.
    }

    private void fibo(int nombreElement) {
        int n1 = 0;
        int n2 = 1;
        int n3;
        System.out.print(n1+" "+n2); // les 2 premières valeurs sont "hardcodées"
        for (int compteur=2;compteur<nombreElement;compteur++){
            n3=n1+n2;
            n1 = n2;
            n2 = n3;
            System.out.print(" "+n3);
        }


        System.out.println(""); //conserver cette ligne.
    }

    /*
     * Modifier le code pour que cette fois-ci la fonctoin fibo2() retourne une chaine de caractère contenant
     * la réponse, et afficher cette réponse dans la fonction appelante.
     *
     *
     */
    public void fonction4() {
        Scanner s = new Scanner(System.in);
        int valeur = s.nextInt();
        System.out.println(fibo2(valeur));

    }

    private String fibo2(int nombreElement) {
        String reponse = "";
        int n1=0;
        int n2=1;
        int n3;
        reponse +=n1+" "+n2;
        for (int compteur=2;compteur<nombreElement;compteur++){
            n3=n1+n2;
            n1=n2;
            n2=n3;
            reponse+=" "+n3;
        }
        return reponse;
    }

    /*
     * Modifiez le code afin afin d'appeler une méthode s'appelant aireRectangle() qui retourne un entier
     * représentant l'aire du rectangle ayant pour cotés les deux valeurs entières passées en paramètres.
     *
     * Pour simplifier l'exercice, les valeurs en entrées sont des entieres positifs (incluant 0).
     *
     * Pour ceux qui ont oublié: l'aire d'un rectangle est largeur * longueur.
     *
     * Exemple:
     * 8 11
     * affichera 88
     *
     */
    public void fonction5() {
        Scanner s = new Scanner(System.in);
        int longueur = s.nextInt();
        int largeur = s.nextInt();
        System.out.println(aireRectangle(longueur, largeur));
    }
    private int aireRectangle(int x,int y) {

        return (x * y);
    }





    /*
     * Ajoutez une méhotde calculant la distance entre 2 points. Cette méthode prend 4 double comme arguments
     * et retourne un double.
     * Appelez cette fonction  distance()
     *
     * Les arguments sont les coordonnées x,y de deux points. C'est donc x1,y1, x2,y2
     *
     * La formule est:
     * dist = racine_carrée( (x2 - x1)aucarré  + (y2 - y1)aucarré )
     *
     * La fonction pour élever aucarré en Java est Math.pow(z,2) (pow est pour power = puissance. Donc
     * ca éléve z à la puissance 2)
     *
     * exemple:
     * 1 2 4 6
     * affichera 5.0
     *
     *
     */
    public void fonction6() {
        Scanner s = new Scanner(System.in);
        double x1 = s.nextDouble();
        double y1 = s.nextDouble();
        double x2 = s.nextDouble();
        double y2 = s.nextDouble();

        System.out.println(distance(x1, y1, x2, y2));


    }


    private double distance(double x1, double y1, double x2, double y2) {
        return(Math.sqrt((Math.pow((x2-x1),2))+Math.pow((y2-y1),2)));
    }


    /*
     * Modifiez le code afin que la fonction estPremier() retourne true ou false selon que le nombre
     * passé en paramètre est un nombre premier ou pas.
     *
     * Pour déterminer si un nombre est premier, if faut vérifier s'il se divise par un entier plus petit que lui.
     * Il n'est pas nécessaire de vérifier tous les entiers, il suffit de se rendre jusqu'à la racine carrée du nombre.
     *
     * Si vous désirez optimiser encore plus votre programme, il n'est pas nécessaire de vérifier les nombres pairs
     * puisque si un nombre se divise par un nombre pair, il est certain qu'il se divise aussi par 2.
     * Il suffit donc de vérifier la division par 2 une fois, et ensuite sauter les nombres pairs.
     *
     * Les cas suivants sont des exceptions: 0 et 1 ne sont pas premiers, et 2 est premier.
     *
     * Exemple:
     * 8
     * affichera:
     * false
     *
     * Exemple 2:
     * 31
     * affichera:
     * true
     */
    public void fonction7() {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();
        System.out.println(estPremier(x));
    }

    private boolean estPremier(int n) {
        boolean estPremier = true; //on présume que c'est un nombre premier jusqu'à preuve du contraire.
        if ( n==0 || n==1) {
            estPremier = false;
        } else if (n==2) {  //un cas d'exception.
            estPremier = true;
        } else {
            for (int compteur=2;compteur<n;compteur++){
                if (n%compteur==0){
                    estPremier= false;
                }
            }
        }
        return estPremier;
    }


    /*
     * Modifiez le code afin que la méthode factorielle() retourne, vous l'avez deviné, la factorielle
     * du nombre passé en paramètre.
     *
     * En mathématique, la factorielle d'un entier naturel n est le produit des nombres entiers strictement
     * positifs inférieurs à n (source: wikipédia)
     *
     * on encore: factorielle(n) = 1*2*3*..*(n-1)*n
     *
     * Par définition, la factorielle de 1 est 1.
     *
     * La factorielle ayant une croissance assez rapide, n'entrez pas de valeur supérieur à 20 car la valeur
     * maximale d'un Long est 9223372036854775807 et la factorielle de 20 est 2432902008176640000. Donc multiplier ca
     * par 21 et vous obtiendrez un nombre supérier a MAX_LONG.
     *
     */
    public void fonction8() {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();
        System.out.println(factorielle(x));
    }

    // >>>>>>> creez la fonction factorielle ici
    private int factorielle(int nombre){
        int resultat=1;
        for (int compteur=nombre; compteur>0;compteur--){
            resultat=compteur*resultat;
        }return (resultat);
    }



    /*
     * Complétez le code afin d'avoir une fonction s'appelant triangle() prenant en paramètre un entier > 0
     * et qui dessine un triangle avec des ^ (le caratère accent circonflexe)
     *
     * exemple:
     * 3
     * affichera:
     * ^
     * ^^
     * ^^^
     *
     */
    public void fonction9() {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();
        triangle(x);
    }

    private void triangle(int n) {
        String lignetriangle= "";
        for (int compteur1=1;compteur1<=n;compteur1++){
                lignetriangle=lignetriangle+"^";
            System.out.println(lignetriangle);

        }
    }
}
